# ABSTRACT

This document describes structure of workspace for PowerDesigner data models processed by this tool.

## Root element of the workspace

By default, data models processed by [App::MDD::Generator::Commit\_Model](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Commit_Model.md) are stored 
in your workspace under key named `PDM` (see option `root`).

## Underlyings tructure

Root element is hashref with the following structure, each key is arrayref 
storing PowerDesigner metaclasses as hashes.

    Root element
        + Tables
        + Views
        + TableShortcuts
        + TableMappings
        + DataSources
        + SourceModels
        + Packages
        + BusinessRules

Elements in the workspace can inherit properties from set of base metaclasses, which are described below.

## Bases metaclasses

### PdmIdentifiedObject

This metaclass provides following properties:

- `Isa` : string, name of the metaclass which is instantiated by this object (eg. `PdmTable`)
- `Id` : string, internal identifier within the PDM file; not needed for most purposes.
- `ObjectID` : string; internal **unique** identifier of the object, eg. `76D0E038-1D04-4EBE-A8EC-36C4FC95EA6C`; not needed for most purposes.

### PdmExtensibleObject - inherited from PdmIdentifiedObject

On top of `PdmIdentifiedObject`, this metaclass provides following properties:

- `ExtendedAttributes` : hashref, where keys are fully qualified (`extension.attribute`) names of extended attributes.

    Please note that only basic types of extended attributes are supported (string, number, boolean, selectlist).

    Do not expect to be able to use complex attribute types, such as: pointer to another object, RTF text field, etc.

    Furthemore, extended attributes of type Boolean are ONLY stored within the PDM if the value is `true`. `False` values
    are not stored in the PDM (and therefore are not stored in your workspace).

    Furhemore, extended attributes that have default value defined in the extension are **not** stored by PowerDesigner 
    in the PDM file, and therefore these defaults are not accessible in your workspace. **Using defaults is not recommended.**

- `Stereotype` : string

### PdmIdentifiedObject - inherited from PdmExtensibleObject

On top of `PdmExtensibleObject`, this metaclass provides following properties:

- `Name` : string
- `Code` : string
- `DisplayName` : string
- `Comment` : string
- `KeywordList` : string
- `AttachedRules` : arrayref

    Property `AttachedRules` is array reference, where each element is reference to a `PdmBusinessRule` 
    attached to the object in question.

## Metaclasses stored under root element

### Tables: metaclass PdmTable

This metaclass is inheritid from `PdmNamedObject`. On top of that, following properties are provided.

- `Columns` : arrayref, elements are instances of `PdmColumn`
- `Keys` : arrayref, elements are instances of `PdmKey`; TODO - to be described.
- `PrimaryKeyRef` : points to `PdmPrimaryKey`; TODO - to be described.

    You should not directly need this, as each column which is part of primary key is labeled is `Primary`.

- `Indexes` : arrayref, elements are instances of `PdmIndex`.

TODO - this needs to be described better.
