# NAME 

App::MDD::Generator::Filters::Basic

# SYNOPSIS

Contains basic filters, that are accessible by default - see how template toolkit 
is initialized in [App::MDD::Generator::Process\_Template](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Process_Template.md).

In your template:

     [% job.job_name | mandatory('job.job_name') %] - rendering dies unless job.job_name is true 
                                                      in Perl sense (defined, not an empty string, non-zero)
                                                      the parameter is identifier of the expression,
                                                      and is printed on output (so that you can identify what 
                                                      was missing, thus crashing the template)
                                                      
     [% job.priority | defaults(1000) %]            - renders 1000 unless job.priority is true 
                                                      in Perl sense

    '[% plan.validator_job_name | sql_escape %]'    - doubles apostrophes so that plan.validator_job_name is valid SQL literal
     
