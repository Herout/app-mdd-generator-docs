# NAME 

App::MDD::Generator::Template::Plugin::PDM

# SYNOPSIS

Plugin which defines several useful virtual methods to navigate a PDM workspace.

In your template:

    [% 
    
    USE PDM;
    
    #------------------------------------------------------------------
    # how to get a column mapping, in several different manners
    #------------------------------------------------------------------
    
    # a) from column, by table mapping name
    #    inputs: column is PdmColumn, map_code is PdmTableMapping.Code (string)
    SET column_mapping = column.colmap_by_mapping(map_code);     
    
    # b) from table mapping, by targtet column code
    #    inputs: mapping is PdmTableMapping, column_code is PdmColumn.Code (string)
    SET column_mapping = mapping.colmap_by_column(column_code);
    
    #------------------------------------------------------------------
    # user friendly access to extended attributes
    #------------------------------------------------------------------    
    
    # one possibility, without virtual method, less user friendly
    # involves more typing    
    SET ea_val = o.ExtendedAttributes.${'extension.attribute'}
    
    # with this plugin
    SET ea_val = o.ea('extension.atribute')
    
    
    #------------------------------------------------------------------
    # how to get and resolve a first source on table mapping or column mapping
    #------------------------------------------------------------------    
    SET source = mapping.source1
    
    %]
    
    
