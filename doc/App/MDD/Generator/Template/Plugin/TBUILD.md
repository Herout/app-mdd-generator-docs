# NAME 

App::MDD::Generator::Template::Plugin::TBUILD

# SYNOPSIS

Plugin which can be used to construct common parts of Teradata TPT script.

In your template:

    [% 
    USE TBUILD;
    
    SET step = {
        tdpid => 'teradata_machine',
        user  => 'load_user',
        pass  => 'password'
    };
        
    SET sql1 = 'delete ....';
    SET sql2 = 'insert into ....';
    SET sql3 = 'update ...';
    
    TBUILD.step_header( 'TRANSFORM_STEP' );
    TBUILD.step( sql1, sql2, sql3 );
    TBUILD.step_footer( step );
    %]
    
    
