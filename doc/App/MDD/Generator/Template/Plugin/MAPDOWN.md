# NAME 

App::MDD::Generator::Template::Plugin::MAPDOWN

# SYNOPSIS

Plugin which contains helpers simplifying outputting mapping description in markdown.

In your template:

    [% 
    
    USE MAPDOWN;
    USE PDM;            # to have .ea virtual method
    
    #------------------------------------------------------------------
    # single line output: TO2_PDM_EDW.ETL_Transformation_Rules = some rules
    # multiline output:   TO2_PDM_EDW.ETL_Transformation_Rules = ->
    #                         ( select ....
    #                           from ......
    #                      <-
    #------------------------------------------------------------------
    %]
    TO2_PDM_EDW.ETL_Transformation_Rules = [% MAPDOWN.Mark( o.ea('TO2_PDM_EDW.ETL_Transformation_Rules') ) %]
    
