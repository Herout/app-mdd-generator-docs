# NAME 

App::MDD::Generator::Role::Logger

# SYNOPSIS

Simplified logging using [Log::Dispatch](https://metacpan.org/pod/Log::Dispatch)

In your [CLI::Osprey](https://metacpan.org/pod/CLI::Osprey) based class:

    use Moo;
    with ('App::MDD::Generator::Role::Logger');
    use CLI::Osprey;

    
    option 'log' => (
        is      =>   'ro',
        format  =>   's',
        repeatable => 1,
        default => sub { [ 'screen,error' ] },
        doc     =>   'OPTIONAL. Logging metadata in format (minlevel,location), where location is either screen, ' .
                    'or full path to file. Example: `--log=info,screen --log=error,generator.log`. ' .
                    'VALID levels are: `debug,info,notice,warning,error,critical,alert,emergency`. ' .
                    'DEFAULT is `screen,error`.'
    );
    
    sub _init {
        my ($self) = @_;
        if (! $self->_logger ) {
            # activate logger using App::MDD::Generator::Role::Logger
            $self->init_logger->_logger->info('Logger was instantiated');
            $self->_logger->info('Now the logger can be used');
        }
        return $self;
    }
    
    sub run {
        my ($self) = @_;
        $self->_init;                       # init logger
        $self->_logger->info('Take care!'); # use the logger
    }
    
