# NAME

Class : App::MDD::Generator::Commit\_Model

## SYNOPSIS

Command line usage

    mdgen --metadata ./metadata commit_model
    

## DESCRIPTION

Purpose of this class is to combine metadata from multiple data models, resolve shortcuts among them, 
and build a coherent workspace out of the metadata.

## OPTIONS

### option root

Root element of the workspace, under which found models are committed. Default is \`PDM\`.
This property should not be modified.

### option reuse\_workspace

If set, existing workspace file will be reused. Otherwise, it will be overwritten.
This property should be used in complex workflows, where PDM files are not the only source of metadata.

### option workspace

Path to the workspace file. 

Workspace file is later used by the [App::MDD::Generator::Process\_Template](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Process_Template.md), and contains metadata from model.
Defaults to `.workspace.storable`, stored under directory which was passed in `metadata` parameter when 
[App::MDD::Generator](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator.md) was invoked.

Workspace file is a binary RAM dump, produced by [Storable](https://metacpan.org/pod/Storable) module. As such, it is **NOT** transferrable to a different
machine, and also should not be subject to version control.

## ALGORITHM

This chapter describes (by means of pseudocode) what is done when data model metadata ara processed.

The algorithm is roughly as follows:

- slurp workspace

    Existing workspace is reused, if option `reuse_workspace` was switched on. Otherwise, the workspace is deleted.

- cache internal Oject IDs

    Data models that were parsed by [App::MDD::Generator::Add\_Models](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Add_Models.md) are scaned to prepare list of internal IDs in each model.

- resolve references in each model

    References in the model are resolved.

- resolve shortcuts among models

    References pointing to other models are resolved.

- destroy internal cache

    List of internal shortcuts is destroyed, as it is no longer needed.

- initialize derived \`primary\` property on table columns
- identify columns that are part of primary key

    Primary keys defined on physical tables are used to set `Primary` property on columns,
    that are part of primary key.

- derive MappedTo from Expression on table&column mappings

    `Expression` on mappings is parsed, internal PowerDesigner tags are stripped, and 
    "clean" `MappedTo` property is prepared for each mapping.

- connect table mappings to target table and table sources

    For both table and column mappings, property `Sources` will 
    contain array of references to objects, that were successfully resolved 
    (see `resolve shortcuts among models` above).

- connect column mappings to target column and source columns

    For table mappings, property `Table` will contain reference to the target table. 

    For column mappings, property `Column` will contain reference to the target column.

- create TableMappings property on level of physical tables

    For each table, create `TableMappings` property as array containing references to each mapping
    which targets this table.

- spew workspace

    Finally, `workspace` file is created.

## LIMITATIONS

At the moment, list of references that were not successfully resolved is dumped to log
with **warning** severity, without any attempt to deduplicate or interpret the list.

This means that **a lot** of warnings will be created, as references to `XEM` and **XBD**
files (at least) will never be resolved.

This limitation should somehow be mitigated in future to identify 
