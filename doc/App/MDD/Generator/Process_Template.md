### Class : App::MDD::Generator::Process\_Template

### SYNOPSIS

Command line usage

    mdgen --metadata ./metadata process_template --template templates/scheduler.tt -selector "{PDM}[]{Tables}[]{Code}(/^MDS_/ && back)"
    

### DESCRIPTION

This class is dedicated to processing of [Template::Toolkit](https://metacpan.org/pod/Template::Toolkit) templates on top of metadata provided 
by other classes of this application. See [App::MDD::WorkspaceStructure](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/WorkspaceStructure.md), and [App::MDD::PDMWorkspaceStructure](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/PDMWorkspaceStructure.md)
for more details.

### Options

#### option 'template\_dir'

This option is path to the root directory of your templates. You are expected to provide a valid path.
This path becomes root for all template processing, which means that [Template::Toolkit](https://metacpan.org/pod/Template::Toolkit) directives 
such as `PROCESS` or `INCLUDE` can use paths relative to the provided `template_dir`.

The option is given to the [Template::Toolkit](https://metacpan.org/pod/Template::Toolkit) object instance as parameter INCLUDE\_PATH.

#### option 'config' 

This option is optional, however recommended to be used. In this option, provide path to a file in YAML format.
The file should have two sections, (project, steps).

\- `project` is a HASH that will be uses as \`c\` object on input of the template.
\- `steps` is an ARRAY of HASHES with keys of \`template\` and \`selector\`. See these options for more details.

Typical use would be as follows: let's imagine that file name `config.yml` was created and contains the following:

    steps:
        - template:     'mapdown/mapdown.tt'
          selector:     '{PDM}[]{TableMappings}[]'
        - template:     'pdc/pdc_lnd.tt'
          selector:     '{PDM}[]{TableMappings}[]'
          
    c:
        output_path:    'xt/40_templates_dump'

        

This means that our generation workflow will contain two steps.

First, template on relative path `mapdown/mapdown.tt` will be processes using selector `{PDM}[]{TableMappings}[]`.

Second, template on relative path `pdc/pdc_lnd.tt` will be processed using the same selector.

Also, our templates will have access to a `c` object declared in the configuration file, meaning we can use constructs 
such as the following in our template.

    [% GET c.output_path %]

#### option 'template' 

Path to the template to be provessed, relative to c&lt;template\_dir>.

#### option 'selector'

Selector path in format of [Struct::Path::PerlStyle](https://metacpan.org/pod/Struct::Path::PerlStyle). This property should be set so that the path points to `arrayref` 
in your workspace. For example: list of tables, list of columns, etc.

Examples:

- `{PDM}[]{TableMappings}[]`
    - List all elements of array under `PDM` key in your workspace (meaning all data models).
    - List all elements of array under `TableMappings` for each model
    - Feed mappings as `o` object to the template
- `{PDM}[]{Code}(eq 'EP_TGT' && back){TableMappings}[]`
    - List all elements of array under `PDM` key in your workspace (meaning all data models).
    - Assess property `Code` of each model in question, and only access the one with `Code eq 'EP_TGT'`
    - List all elements of array under `TableMappings` for each model
    - Feed mappings as `o` object to the template (meaning only mappings in data model EP\_TGT will be given to template)
- `{PDM}[]{Code}(eq 'EP_TGT' && back){TableMappings}[]{Code}(/PARTY/ && back)`
    - List all elements of array under `PDM` key in your workspace (meaning all data models).
    - Assess property `Code` of each model in question, and only access the one with `Code eq 'EP_TGT'`
    - List all elements of array under `TableMappings` for each model
    - Assess property `Code` for each mapping, and only access those which contain string `PARTY` in the `Code`
    - Feed those mappings as `o` object to the template (meaning only mappings in data model EP\_TGT with name "like PARTY" will be given to template)

### Template structure

The template should always contain two sections, divided by line containing only sequence of dashes.
Example template:

    {
        "filename"   :     "[% c.output_path %]/[% o.Code %].sql",
        "activation" :     "1"
    }
    ---
    Template content - file which will be written to [% c.output_path %]\[% o.table_name %].sql

The first section is a simple JSON string, which is expected to contain two parameters.

- filename

    This value us the name of file, which is to be created and which will store contents of the processed template.

- activation

    This value should be either true or false, in Perl-ish sense. If the value is **false**, 
    then the processed template will **not be stored** in the `filename` provided (template will be processed normally, 
    but its result will be thrown away).

### Template::Toolkit object initialization

The class creates a [Template::Toolkit](https://metacpan.org/pod/Template::Toolkit) object in the following fashion.

    my $tt = Template->new(
        {   INCLUDE_PATH => $self->template_dir->stringify,
            INTERPOLATE  => 1,
            EVAL_PERL    => 1,
            ENCODING     => $self->template_encoding,
            PLUGIN_BASE => $self->config_plugin_base,
            FILTERS => {
                # dynamic filters
                defaults => [ \&App::MDD::Generator::Filters::Basic::defaults_factory, 1 ],
                mandatory => [ \&App::MDD::Generator::Filters::Basic::mandatory, 1 ],
                
                # static filters
                sql_escape => [ \&App::MDD::Generator::Filters::Basic::sql_escape, 0 ]
            },
            RELATIVE => 1,
        }
    ) || confess "$Template::ERROR\n";


    
