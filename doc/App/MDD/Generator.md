# NAME 

App::MDD::Generator 

# SYNOPSIS

Command line usage

    mdgen --metadata ./metadata add_models --models ./input/models
    mdgen --metadata ./metadata commit_models
    mdgen --metadata ./metadata process_template --template templates/scheduler.tt -selector "{PDM}[]{Tables}[]{Code}(/^MDS_/ && back)"
    mdgen --metadata ./metadata process_template --config ./config/generator_config.yaml

Batch workflow usage, supporting dependency injection via [Beam::Wire](https://metacpan.org/pod/Beam::Wire).

    mdrun ./config/workflow.yaml all


    

# DESCRIPTION

This application provides code generator built on top of [Template Toolkit](http://template-toolkit.org/). 

It is capable of:

- utilising metadata from data models 

    Data models in PDM (SAP Sybase PowerDesigner) format ar supported, 
    by means of the `add_model` and `commit_model` subcommands.

- utilising metadata from files in YAML format

    YAML format metadata can be slurped by means of the `comit_yaml` subcommand

- processing templates written for Template Toolkit

    See [http://template-toolkit.org/](http://template-toolkit.org/) for more details. Subcommand is `process_template`.

- processing of simple workflow with dependency injection 

    This is implemented by `run` utility, and uses `Beam::Wire` module.

# OPTIONS

## option 'metadata'

Directory where metadata from data models are stored. Metadata are created by means of running \`add\_model\` and \`commit\_workspace\` subcommands.                
Metadata are then used by the \`process\_template\` subcommand to actually generate outputs.'

## option 'log' 

**OPTIONAL.** Logging metadata in format (minlevel,location), where location is either  
\`screen\`, or full path to file. 

Example when run from comand line.

    --log=info,screen --log=error,generator.log

When executed from within Perl program, for example when an instance of \`App::MDD:Generator\` is created using \`Beam::Wire\`, 
this property stores \`ARRAYREF\`.

For example:

    my $gen = App::MDD:Generator->new ( 
        metadata => "$dir_meta",
        log => [ 
            "screen,error" 
          , "debug.log,debug"
            ]
    );

Valid levels are: 

- debug
- info
- notice
- warning
- error
- critical
- alert
- emergency

**DEFAULT** is \`screen,error\`.

# Subcommands 

## subcommand process\_template

See [App::MDD::Generator::Process\_Template](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Process_Template.md)

## subcommand add\_models

See [App::MDD::Generator::Add\_Model](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Add_Model.md)

## subcommand commit\_models

See [App::MDD::Generator::Commit\_Model](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Commit_Model.md)

## subcommand commit\_yaml

See [App::MDD::Generator::Commit\_Yaml](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Commit_Yaml.md)

# Workspace structure

- [App::MDD::WorkspaceStructure](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/WorkspaceStructure.md)
- [App::MDD::PDMWorkspaceStructure](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/PDMWorkspaceStructure.md)

# Plugins, filters, virtual methods

This project also implements set of filters, and plugins, that are accessible to you.

See following items for more details:

- [App::MDD::Generator::Template::Plugin::COALESCE](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Template/Plugin/COALESCE.md)
- [App::MDD::Generator::Template::Plugin::MAPDOWN](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Template/Plugin/MAPDOWN.md)
- [App::MDD::Generator::Template::Plugin::PDC](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Template/Plugin/PDC.md)
- [App::MDD::Generator::Template::Plugin::PDM](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Template/Plugin/PDM.md)
- [App::MDD::Generator::Template::Plugin::TBUILD](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Template/Plugin/TBUILD.md)

# LICENSE

Copyright (c) 2018 Jan Herout <jan.herout@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 

# SUPPORT

Support of this software is provided as paid service, on subscription basis.
Contact author of the software whenever needed.

# LIMITATIONS

This software bundles parser of PDM files, which is compiled binary program.
The parser is used from within [App::MDD::Generator::Add\_Model](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Add_Model.md), and is restricted
to parsing of data models limited up to 10 physical tables.

This restriction is lifted by acquiring support, on subscription basis.

# AUTHOR

jan-herout <jan.herout@gmail.com>
