# ABSTRACT

This document describes structure of workspace processed by this tool.

## General idea

When you run [App::MDD::Generator::Add\_Model](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Add_Model.md) and later on [App::MDD::Generator::Commit\_Model](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Commit_Model.md), 
what is being done on the background is creation of a workspace that can be used by 
[App::MDD::Generator::Process\_Template](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Process_Template.md).

The workspace is effectively a Perl hash reference (see [perldata](https://perldoc.perl.org/perldata.html)),
where each type of metadata is stored under a separate key. The workspace is effectively a hierarchy, 
where each root key in the hierarchy is used to store one type of metadata. 

In the simplest case, workspace contains only one key, `PDM`, and this key points to an array of data models
that were processed by the [App::MDD::Generator::Commit\_Model](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Commit_Model.md) class. 

The workspace can be conceptualized as something approximated by this ascii art.

    Workspace                               - hashref
      +-- PDM                               - arrayref
            +-- Model_1                     - hashref
                  + Name, Code, Stereotype  - scalars
                  + Packages                - arrayref
                  + Tables                  - arrayref
                  + Views                   - arrayref
                  + TableMappings           - arrayref
                  

This means, that in plain perl, one could navigate the workspace in manner simillar to this:

    # pointer to model with code EP_TGT
    my $mdl_tgt   = grep { $_->{Code} eq 'EP_TGT' } $workspace->{PDM}->@*;    
    
    # pointer to table PARTY inder model EP_TGT
    my $tab_party = grep { $_->{Code} eq 'PARTY' } $mdl_tgt->{Tables}->@*;
    
    # list of mappings where target table is PARTY
    my @map_party = grep { $_->{Table}->{Code} eq 'PARTY' } $mdl_tgt->{TableMappings}->@*;
    my @map_party = $tab_party->{Mappings}->@*;     
    
    

It is important to know what is and what is NOT accessible in the workspace to be able to 
construct templates effectively.

The workspace does **NOT** contain all types of objects and/or their properties, that
are available in the data modelling tool, however, it contains most of them 
(effectively those that were proven to be useful during 3 years experience of model driven development
on data warehouse in major Telco company).

Most of properties can be accessed directly, however, for some of them it seemed more effective to
prepare set of new [template toolkit virtual methods](http://www.template-toolkit.org/docs/manual/VMethods.html).

This project also implements set of filters, and plugins, that are accessible to you.

See following items for more details:

- [App::MDD::Generator::Template::Plugin::COALESCE](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Template/Plugin/COALESCE.md)
- [App::MDD::Generator::Template::Plugin::MAPDOWN](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Template/Plugin/MAPDOWN.md)
- [App::MDD::Generator::Template::Plugin::PDC](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Template/Plugin/PDC.md)
- [App::MDD::Generator::Template::Plugin::PDM](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Template/Plugin/PDM.md)
- [App::MDD::Generator::Template::Plugin::TBUILD](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/Generator/Template/Plugin/TBUILD.md)

The following text describes what can be accessed in your workspace.

## Properties of PowerDesigner data models 

See [App::MDD::PDMWorkspaceStructure](https://gitlab.com/Herout/app-mdd-generator/tree/master/doc/App/MDD/PDMWorkspaceStructure.md) for more details.
